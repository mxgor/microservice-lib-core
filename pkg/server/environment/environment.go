package environment

type DefaultEnv struct {
	healthFunc func() bool
}

func NewDefaultEnv() *DefaultEnv {
	return &DefaultEnv{
		healthFunc: func() bool {
			return true
		},
	}
}

func (e *DefaultEnv) IsHealthy() bool {
	return e.healthFunc()
}

func (e *DefaultEnv) SetHealthFunc(fn func() bool) {
	e.healthFunc = fn
}
